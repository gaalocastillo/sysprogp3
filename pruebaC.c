#include <stdio.h>
#include <stdlib.h> 
#include <ctype.h>
#define SIZE 30

 main(){
 	int numPersonas = 10;
 	
 	int clean_stdin();
 	float totalEdades = 0;
 	int edadMayor = 0;
 	int edadMenor = 0;
 	char nomPersMayor=(char) malloc( 100 );
 	char apellPersMayor=(char) malloc( 100 );
	char nomPersMenor=(char) malloc( 100 );
 	char apellPersMenor=(char) malloc( 100 );

 	for(int i = 1; i<=numPersonas; i = i+1){
 		char nombre=(char) malloc( 100 );
 		char apellido=(char) malloc( 100 );

 		printf("\nPersona %d:", i);
 		do
	    {  
	        printf("\nIngrese su nombre: \n");
	        scanf("%s", nombre);
	    } while ((validoNombre(nombre) == 0) &&  clean_stdin());


 		do
	    {  
	        printf("\nIngrese su apellido: \n");
	        scanf("%s", apellido);
	    } while ((validoNombre(apellido) == 0) && clean_stdin());

	    int edad = 0;  
	    char c;
	    do
	    {  
	        printf("\nIngrese su edad: \n");

	    } while (((scanf("%d%c", &edad, &c)!=2 || c!='\n') && clean_stdin()) || edad<1 || edad>23);
	
	    if(i == 0){
	    	edadMenor = edad;
	    	nomPersMenor = nombre;
	    	apellPersMenor = apellido;
	    }

	    if(edad < edadMenor){
			edadMenor = edad;
	    	nomPersMenor = nombre;
	    	apellPersMenor = apellido;
		}
		if (edad > edadMayor){
			edadMayor = edad;
			nomPersMayor = nombre;
			apellPersMayor = apellido;
		}
		totalEdades = totalEdades + edad;
	}

	float promEdades = totalEdades/numPersonas;
	printf("Promedio de edad = %f \n", promEdades);
	printf("Persona mas vieja: %s %s, edad = %d \n", nomPersMayor, apellPersMayor, edadMayor);
	printf("Persona mas joven: %s %s, edad %d \n", nomPersMenor, apellPersMenor, edadMenor);
}


int validoNombre (char *str)
{
  int contador=1;
  if(isalpha(*str)==0||islower(*str)!=0){
    printf("Debe empezar con mayúscula  \n");
    return 0;
  }
  str+=1;
  while(*str!='\0'){
    
    if(isalpha(*str)==0 || islower(*str)==0){
      printf("No debe ser un caracter especial o dígito o mayúscula \n");
      return 0;
    }
    str++;  
    contador++;  
  }
  if(contador<2){
    return 0;
  }
  return 1;
}

int clean_stdin(){
    while (getchar()!='\n');
    return 1;
}

